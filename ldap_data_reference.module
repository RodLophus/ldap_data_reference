<?php

/**
 * Implements hook_field_info().
 */
function ldap_data_reference_field_info() {
	$fields = array(
		'ldap_data_reference' => array(
			'label' => t('LDAP Data Reference'),
			'description' => t('Offers options based on values returned by a LDAP query.'),
			'settings' => array(
				'max_length' => 255
			),
			'instance_settings' => array(
				'ldap_data_reference_query' => '',
				'ldap_data_reference_attribute' => ''
			),
			'default_widget' => 'ldap_data_reference_select_widget',
			'default_formatter' => 'ldap_data_reference_values_formatter'
		)
	);
	return $fields;
}


/**
 * Implements hook_field_instance_settings_form().
 */
function ldap_data_reference_field_instance_settings_form($field, $instance) {
	module_load_include('inc', 'ldap_data_reference', 'ldap_data_reference.util');

	$settings = $instance['settings'];

	$form = array();
	$form['ldap_data_reference_query'] = array(
		'#type' => 'select',
		'#title' => t('LDAP query'),
		'#default_value' => $settings['ldap_data_reference_query'],
		'#options' => ldap_data_reference_get_ldap_query_list(),
		'#required' => TRUE
	);

	switch($instance['widget']['type']) {
		case 'ldap_data_reference_select_widget':
			$form['ldap_data_reference_attribute'] = array(
				'#type' => 'textfield',
				'#title' => t('LDAP attributes to build the select list'),
				'#default_value' => $settings['ldap_data_reference_attribute'],
				'#element_validate' => array('ldap_data_reference_field_instance_settings_validate'),
				'#required' => TRUE
			);
		break;
		case 'ldap_data_reference_autocomplete_widget':
			$form['ldap_data_reference_attribute'] = array(
				'#type' => 'textfield',
				'#title' => t('LDAP attribute to search'),
				'#default_value' => explode('|', $settings['ldap_data_reference_attribute'])[1],
				'#element_validate' => array('ldap_data_reference_field_instance_settings_validate'),
				'#required' => TRUE
			);
		break;
	}

	return $form;
}


/**
 * Config field's configuration form validation function
 */
function ldap_data_reference_field_instance_settings_validate($element, &$form_state) {
	$ldap_attr = $form_state['values']['instance']['settings']['ldap_data_reference_attribute'];

	if(! preg_match('/^[a-z0-9_\-]+(\|[a-z0-9_\-]+)?$/i', $ldap_attr)) {
		form_set_error('ldap_data_reference_attribute', t('Invalid LDAP attribute(s).'));
		return;
	}

	if(! strpos($ldap_attr, '|')) {
		$form_state['values']['instance']['settings']['ldap_data_reference_attribute'] = $ldap_attr .'|' . $ldap_attr;
	}
}


/**
 * Implements hook_field_widget_info().
 */
function ldap_data_reference_field_widget_info() {
	return array(
		'ldap_data_reference_select_widget' => array(
			'label' => t('Select list'),
			'field types' => array('ldap_data_reference'),
			'behaviors' => array(
				'default value' => FIELD_BEHAVIOR_NONE,
			)
		),
		'ldap_data_reference_autocomplete_widget' => array(
			'label' => t('Autocomplete text field'),
			'field types' => array('ldap_data_reference'),
			'behaviors' => array(
				'default value' => FIELD_BEHAVIOR_NONE,
			)
		)
	);
}


/**
 * Implements hook_field_widget_form().
 */
function ldap_data_reference_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
	module_load_include('inc', 'ldap_data_reference', 'ldap_data_reference.util');
	$query_name = $instance['settings']['ldap_data_reference_query'];
	$ldap_attributes = explode('|', strtolower($instance['settings']['ldap_data_reference_attribute']));

print_r($items);

	switch ($instance['widget']['type']) {
		case 'ldap_data_reference_select_widget' :
			// Select list widget will supply $key.  We will get $value aftwards (on hook_field_presave())
			$element['key'] = array(
				'#type' => 'select',
				'#title' => $element['#title'],
				'#default_value' => isset($items[$delta]['key']) ? $items[$delta]['key'] : '',
				'#options' => ldap_data_reference_get_ldap_data($query_name, $ldap_attributes[1], $ldap_attributes[0])
			);
		break;
		case 'ldap_data_reference_autocomplete_widget' :
			// Autocomplete widget will supply $value.  We will get $key aftwards (on hook_field_presave())
			$element['value'] = array(
				'#type' => 'textfield',
				'#title' => $element['#title'],
				
			);
		break;
	}

	return $element;
}


/**
 * Implements hook_field_is_empty().
 */
function ldap_data_reference_field_is_empty($item, $field) {
	return empty($item['key']) && empty($item['value']);
}


/**
 * Implements hook_field_formatter_info().
 */
function ldap_data_reference_field_formatter_info() {
	return array(
		'ldap_data_reference_values_formatter' => array(
			'label' => t('Field value'),
			'field types' => array('ldap_data_reference')
		),
		'ldap_data_reference_keys_formatter' => array(
			'label' => t('Field key'),
			'field types' => array('ldap_data_reference')
		),
	);
}


/**
 * Implements hook_field_formatter_view().
 */
function ldap_data_reference_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
	$element = array();
	switch ($display['type']) {
		case 'ldap_data_reference_values_formatter':
			foreach ($items as $delta => $item) {
				if (isset($item['value'])) {
					$output = field_filter_xss($item['value']);
				} else {
					$output = '';
				}
				$element[$delta] = array('#markup' => $output);
			}
		break;
		case 'ldap_data_reference_keys_formatter':
			foreach ($items as $delta => $item) {
				if (isset($item['key'])) {
					$output = field_filter_xss($item['key']);
				} else {
					$output = '';
				}
				$element[$delta] = array('#markup' => $output);
			}
		break;
		}
	return $element;
}


/**
 * Implements hook_field_presave().
 */
function ldap_data_reference_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items) {
	module_load_include('inc', 'ldap_data_reference', 'ldap_data_reference.util');
	$query_name = $instance['settings']['ldap_data_reference_query'];
	$ldap_attributes = explode('|', strtolower($instance['settings']['ldap_data_reference_attribute']));

	foreach ($items as $delta => $item) {
		if(empty($items[$delta]['value']) && isset($item['key'])) {
			// Search LDAP for $key and fill in $value
			$items[$delta]['value'] = ldap_data_reference_get_ldap_data($query_name, $ldap_attributes[1], $ldap_attributes[0], $items[$delta]['key']);
		} elseif(empty($items[$delta]['key']) && isset($item['value'])) {
			// Search LDAP for $value and fill in $key
			$items[$delta]['key'] = ldap_data_reference_get_ldap_data($query_name, $ldap_attributes[0], $ldap_attributes[1], $items[$delta]['value']);
		}
	}
}

