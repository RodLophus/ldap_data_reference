<?php

function ldap_data_reference_get_ldap_query_list() {
	$result = array();
	$queries = ldap_query_get_queries(NULL, 'enabled');
	foreach($queries as $key => $query) {
		$result[$key] = $query->name;
	}
	natsort($result);
	return $result;
}


function ldap_data_reference_get_ldap_data($query_name, $value_attr, $key_attr = FALSE, $key_value = FALSE) {
	$result = array();
	$query = ldap_query_get_queries($query_name, 'enabled', TRUE);

	if($key_value) {
		// Key value has been specifyed: return only matching results
		$query->filter = '(|' . $query_filter . '(' . $key_attr . '=' . $key_value . '))';
	}

	$query_result = $query->query();

	if(! empty($query_result)) {
		for($i = 0; $i < $query_result['count']; $i++) {
			if(! empty($query_result[$i][$value_attr][0])) {
				// Will return array($key_attr => $value_attr)
				if($key_attr) {
					if(! empty($query_result[$i][$key_attr][0])) {
						$result[$query_result[$i][$key_attr][0]] = $query_result[$i][$value_attr][0];
					}
				} else {
					// Will return array($value_attr)
					$result[] = $query_result[$i][$value_attr][0];
				}
			}
		}
	}

	natsort($result);
	return $result;
}
